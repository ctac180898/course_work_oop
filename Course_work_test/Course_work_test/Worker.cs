﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Course_work_test
{

    enum Positions { Worker, Admin, Err}

    class Worker { 
        private Positions position;
        public string name;
        private string login;
        private string password;

        public Worker()
        {
            name = "err";
            login = "err";
            password = "err";
            position = Positions.Err;
        }

        public bool CheckWorker(string log, string pass)
        {
            return login == log && password == pass;
        }

        public Worker(string name, string log, string pass, Positions pos)
        {
            this.name = name;
            this.login = log;
            this.password = pass;
            this.position = pos;
        }

        public Positions Position { get { return position; } }

        public Worker CreateWorker(string name, string log, string pass, Positions pos)
        {
            return new Worker(name, log, pass, pos); 
        }

        public void PrintInfo()
        {
            Console.WriteLine("Name: {0}, Login: {1}", name, login);
        }
    }
}
