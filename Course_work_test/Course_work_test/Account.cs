﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Course_work_test
{

    public abstract class Account : IEquatable<Account>
    {
        protected double money;
        protected string login;
        protected string password;

        public virtual string GetAccLog()
        {
            string hiddenlog = login;
            return hiddenlog;
        }


        public Account()
        {
            money = 0;
            login = "DefLog";
            password = "DefPass";
        }
        
        public virtual void Print()
        {
            Console.WriteLine("Login: {0} Password: {1} Money: {2}", login, password, money);
        }

        public virtual bool CheckAcc(string log, string pass)
        {
            return login == log && password == pass;
        }

        public bool CheckAcc(Account acc)
        {
            return login == acc.login && password == acc.password;
        }

        override
        public bool Equals(object obj)
        {
            if (obj == null) return false;
            Account objAsAcc = obj as Account;
            if (objAsAcc == null) return false;
            else return Equals(objAsAcc);
        }

        public bool Equals(Account other)
        {
            if (other == null) return false;

            return (this.login == other.login && this.password == other.password);
        }


        public virtual double MoneyAmount() { return -1; }
        public virtual void ZeroEarned() { }
        public virtual double TakeEarnedMoney(double cash) { return -1; }
        public virtual double EarnedMoneyAmount() { return -1; }
        public virtual double PutMoney(double cash) { return -1; }
        public virtual double TakeMoney(double cash) { return -1; }
        public virtual void Update() { }
        public virtual double Percent() { return -1; }
    }

    class ErrAcc : Account
    {
        public ErrAcc()
        {
            login = "Error";
            password = "Error";
        }
        
    }

    class DAccount : Account
    {

        public DAccount(string log, string pass)
        {
            login = log;
            password = pass;
        }

        override
        public double EarnedMoneyAmount(){ return -1; }
        override
        public double TakeEarnedMoney(double cash) { return -1; }
        override
        public void Update(){ }

        override
        public void ZeroEarned()
        { }

        override
        public double MoneyAmount()
        {
            return this.money;
        }

        override
        public double PutMoney(double cash)
        {
            money += cash;
            return money;
        }

        override
        public double TakeMoney(double cash)
        {
            if(money >= cash)
            {
                money -= cash;
                return cash;
            }
            return -1;
        }
    }

    class Deposit : Account
    {
        protected double percentage;
        protected double EarnedMoney;

        public Deposit(string log, string pass, double percent)
        {
            login = log;
            password = pass;
            EarnedMoney = 0;
            percentage = percent;
        }

        override
        public double MoneyAmount()
        {
            return this.money;
        }
        
        override
        public void ZeroEarned()
        {
            EarnedMoney = 0;
        }

        override
        public double EarnedMoneyAmount()
        {
            return EarnedMoney;
        }

        override
        public double PutMoney(double cash)
        {
            money += cash;
            return money;
        }

        override
        public double TakeMoney(double cash)
        {
            if(money >= cash)
            {
                money -= cash;
                return cash;
            }
            return -1;
                
        }

        override
        public  double Percent()
        { return percentage; }

        override
        public double TakeEarnedMoney(double cash)
        {
            if(EarnedMoney >= cash)
            {
                EarnedMoney -= cash;
                return cash;
            }
            return -1;
        }

        override
        public void Update()
        {
            Console.WriteLine("Earned money: {0}, percentage: {1}, money: {2}", EarnedMoney, percentage, money);
            EarnedMoney = EarnedMoney + ((percentage * (money + EarnedMoney))/100);
            Console.WriteLine("Earned money: {0}, percentage: {1}, money: {2}", EarnedMoney, percentage, money);
            Console.ReadKey();
        }
    }

    class Creator
    {
        public Account NewAcc(string accType, string log, string pass, double percent)
        {
            if(accType.ToLower() == "deposit")
            {
                return new Deposit(log, pass, percent);
            }
            else
            {
                return new DAccount(log, pass);
            }
        }
    }

    

    public abstract class AccDecorator : Account, IEquatable<Account>
    {
        protected Account acc;

        override
        public bool Equals(object obj)
        {
            if (obj == null) return false;
            Account objAsAcc = obj as Account;
            if (objAsAcc == null) return false;
            else return Equals(objAsAcc);
        }

        public bool Equals(Account other)
        {
            if (other == null) return false;

            return other.CheckAcc(acc);
        }

        override
        public string GetAccLog()
        {
            return acc.GetAccLog();
        }

        override
        public bool CheckAcc(string log, string pass)
        {
            return acc.CheckAcc(log, pass);
        }

        override
        public void Print()
        {
            acc.Print();
        }

        public void AddAccProp(Account baseAcc)
        {
            if(baseAcc is Deposit)
            {
                this.acc = baseAcc;
            } 
        }

        override
        public double MoneyAmount()
        {
            if (acc != null)
                return acc.MoneyAmount();
            else
                return -1;
        }

        override
        public double EarnedMoneyAmount()
        {
            return acc.EarnedMoneyAmount();
        }

        override
        public void ZeroEarned()
        { }

        override
        public double PutMoney(double cash)
        {
            if (acc != null) return acc.PutMoney(cash);
            else return -1;
        }

        override
        public double TakeMoney(double cash)
        {
            if (acc != null) return acc.TakeMoney(cash);
            else return -1;
        }

        override
        public void Update()
        {
            acc.Update();
        }

        override
        public double TakeEarnedMoney(double cash)
        {
            if (acc != null)
            {
                return acc.TakeEarnedMoney(cash);
            }
            else return -1;
        }

        override
        public double Percent()
        {
            return acc.Percent();
        }
    }

    public class OutOfTurnMoneyTakeDep : AccDecorator
    {
        override
        public double TakeMoney(double cash)
        {
            if (acc != null)
            {
                return acc.TakeMoney(cash);
            }
            else return -1;
        }
    }

    public class CommonDep : AccDecorator
    {
        int neededupdates = 5;
        int updates = 0;

        override
        public double TakeMoney(double cash)
        {
            if (acc != null)
            {
                if(updates % neededupdates != 0)
                {
                    acc.ZeroEarned();
                }
                
                return acc.TakeMoney(cash);
            }
            else return -1;
        }

        override
        public void Update()
        {

            updates++;
            acc.Update();
        }
    }
}