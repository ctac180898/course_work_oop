﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Course_work_test
{
    class UserSystem
    {

        private static UserSystem _usystem;
        LoggedUserState lus = new LoggedUserState();
        NotLoggedUserState nlus = new NotLoggedUserState();

        private UserState curState;

        List<Worker> workers;

        protected UserSystem()
        {
            curState = nlus;
            workers = new List<Worker>();
            workers.Add(new Worker("Stas", "log", "pass", Positions.Admin));
        }

        public string UserName()
        {
            return lus.loggedWorker.name;
        }

        public bool IsLogged()
        {
            return curState == lus; ;
        }

        public bool IsLoggedAdmin()
        {
            return curState.Position() == Positions.Admin;
        }

        public bool CreateWorker(string log, string pass, string name, string pos)
        {
            if (this.IsLoggedAdmin())
            {
                Positions position;
                if (pos == "admin")
                    position = Positions.Admin;
                else
                    position = Positions.Worker;
                workers.Add(curState.CreateWorker(name, log, pass, position));

                return true;
            }
            return false;
        }

        public static UserSystem UsSystem()
        {
            if (_usystem == null)
            {
                _usystem = new UserSystem();
            }
            return _usystem;
        }

        public bool Login(string log, string pass)
        {
            Worker worker = workers.Find(x => x.CheckWorker(log, pass));
            if (worker != null)
            {
                curState = lus;
                curState.Login(worker);
                return true;
            }
            return false;
        }

        public void Logout()
        {
            curState.loggedWorker = null;
            curState = nlus;
        }

        public void PrintUsInfo()
        {
            if (curState == lus)
                curState.loggedWorker.PrintInfo();
        }
    }

    abstract class UserState
    {
        public string stname;
        public Worker loggedWorker;
        abstract public Worker CreateWorker(string name, string log, string pass, Positions pos);
        abstract public Positions Position();
        abstract public void Login(Worker worker);
    }

    class LoggedUserState : UserState
    {

        public LoggedUserState()
        {
            stname = "LoggedIn";
        }

        override
        public void Login(Worker worker)
        {
            loggedWorker = worker;
        }

        override
        public Worker CreateWorker(string name, string log, string pass, Positions pos)
        {
            return loggedWorker.CreateWorker(name, log, pass, pos);
        }

        override
        public Positions Position()
        {
            return loggedWorker.Position;
        }
    }

    class NotLoggedUserState : UserState
    {
        public NotLoggedUserState()
        {
            stname = "NotLoggedIn";
        }

        override
        public Worker CreateWorker(string name, string log, string pass, Positions pos)
        {
            return new Worker();
        }

        override
        public void Login(Worker worker)
        {

        }

        override
        public Positions Position()
        {
            return loggedWorker.Position;
        }
    }
}
