﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Course_work_test
{
    public class Bill
    {
        public string header;
        public string footer;
        public string content;

        public void Print()
        {
            Console.WriteLine(header);
            Console.WriteLine(content);
            Console.WriteLine(footer);
        }
    }

    public abstract class BillBuilder
    {
        public Bill bill;
        public void CreateBill()
        {
            bill = new Bill();
        }

        public abstract void SetHeader(string accLog);
        public abstract void SetFooter();
        public abstract void SetContent(string money);

        public Bill DispatchBill()
        {
            return bill;
        }
    }

    public class CashTakeBill : BillBuilder
    {

        override
        public void SetHeader(string accLog)
        {
            bill.header = "Acc: " + accLog;
        }

        override
        public void SetFooter()
        {
            bill.footer = "Date: " + DateTime.Today.ToString();
        }

        override
        public void SetContent(string money)
        {
            bill.content = "Taken from acc: " + money + " hrn";
        }

    }

    public class CashPutBill : BillBuilder
    {

        override
        public void SetHeader(string accLog)
        {
            bill.header = "Acc: " + accLog;
        }

        override
        public void SetFooter()
        {
            bill.footer = "Date: " + DateTime.Today.ToString();
        }

        override
        public void SetContent(string money)
        {
            bill.content = "Put on acc: "  + money + " hrn";
        }

    }

    public class Director
    {
        public Bill GenerateBill(BillBuilder billBuilder, string accLog, string money)
        {
            billBuilder.CreateBill();
            billBuilder.SetHeader(accLog);
            billBuilder.SetFooter();
            billBuilder.SetContent(money);
            return billBuilder.DispatchBill();
        }
    }
}
