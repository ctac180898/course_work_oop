﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Course_work_test
{
    

    class BankSystem
    {
        private static BankSystem _system;

        public enum AccSystemStateSetting
        {
            Logged,
            NotLogged
        };

        Director dir = new Director();

        ControlledAccountSystem controlledAcc = new ControlledAccountSystem();

        UserSystem userSystem =  UserSystem.UsSystem();

        protected BankSystem()
        {
            
        }

        public static BankSystem BankSys()
        {
            if(_system == null)
            {
                _system = new BankSystem();
            }
            return _system;
        }

        public void Login()
        {
            Console.Clear();
            Console.WriteLine("Enter login: ");
            string log = Console.ReadLine();
            Console.WriteLine("Enter password: ");
            string pass = Console.ReadLine();
            bool result =  userSystem.Login(log, pass);
            if (result)
            {
                Console.Clear();
                Console.WriteLine("You are now logged in worker, tap any key to continue");
                Console.ReadKey();
            }
            else
            {
                Console.Clear();
                Console.WriteLine("Invalid login or password, tap any key to continue");
                Console.ReadKey();
            }
        }

        public void Logout()
        {
            if (userSystem.IsLogged())
            {
                Console.Clear();
                userSystem.Logout();
                Console.WriteLine("You`ve been logged out");

                Console.WriteLine("Tap any key to continue");
                Console.ReadKey();
            }
            else
            {
                Console.Clear();
                Console.WriteLine("You are logged out");

                Console.WriteLine("Tap any key to continue");
                Console.ReadKey();
            }
        }

        public void AccLogin()
        {
            Console.Clear();
            Console.WriteLine("Enter log: ");
            string log = Console.ReadLine();
            Console.WriteLine("Enter pass: ");
            string pass = Console.ReadLine();

            bool result = controlledAcc.Login(log, pass);
            if (result)
            {
                Console.Clear();
                Console.WriteLine("You are now logged in account, tap any key to continue");
                Console.ReadKey();
            }
            else
            {
                Console.Clear();
                Console.WriteLine("Invalid login or password, tap any key to continue");
                Console.ReadKey();
            }
        }

        public void AccLogout()
        {
            controlledAcc.Logout();
        }

        public void PrintUsInfo()
        {
            if (userSystem.IsLogged())
                userSystem.PrintUsInfo();
            else
                Console.WriteLine("You are not logged in");
            Console.WriteLine("Tap any key to continue");
            Console.ReadKey();
        }

        public void PrintAccInfo()
        {
            if (controlledAcc.IsLogged())
                controlledAcc.PrintLoggedAccInfo();
            else
                Console.WriteLine("You are not logged in acc");
            Console.WriteLine("Tap any key to continue");
            Console.ReadKey();
        }

        public void CreateAcc()
        {
            if (userSystem.IsLoggedAdmin())
            {
                double percent = 0;
                Console.Clear();
                Console.WriteLine("Enter login ( > 3 symbols): ");
                string log = Console.ReadLine();
                Console.WriteLine("Enter password ( > 3 symbols): ");
                string pass = Console.ReadLine();
                Console.WriteLine("Enter acc type(deposit or default): ");
                string accType = Console.ReadLine();

                if(accType == "deposit")
                {
                    Console.WriteLine("Enter deposit percentage: ");
                    string percentage = Console.ReadLine();
                    percent = Double.Parse(percentage);
                }

                bool result = controlledAcc.CreateAcc(accType, log, pass, percent);

                if (result)
                {
                    Console.Clear();
                    Console.WriteLine("Account created, tap any key to continue");
                    Console.ReadKey();
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("Account with this log and pass already exists or invalid log and pass, tap any key to continue");
                    Console.ReadKey();
                }
            }
        }

        public void AddAccProp()
        {
            if (userSystem.IsLoggedAdmin())
            {
                Console.WriteLine("Enter prop name(OutOfTurnMoneyTakeDep or CommonDep): ");
                string prop = Console.ReadLine();
                bool result =  controlledAcc.AddAccProp(prop);
                if (result)
                {
                    Console.Clear();
                    Console.WriteLine("Property added successfuly");
                    Console.ReadKey();
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("Invalid account type or you are not an admin");
                    Console.ReadKey();
                }
            }
        }


        public void CreateWorker()
        {
            Console.Clear();
            Console.WriteLine("Enter login: ");
            string log = Console.ReadLine();
            Console.WriteLine("Enter password: ");
            string pass = Console.ReadLine();
            Console.WriteLine("Enter name: ");
            string name = Console.ReadLine();
            Console.WriteLine("Enter position(admin or worker): ");
            string pos = Console.ReadLine();

            bool result =  userSystem.CreateWorker(log, pass, name, pos);
            if (result)
            {
                Console.Clear();
                Console.WriteLine("Wroker added to system successfuly, tap any key to continue");
                Console.ReadKey();
            }
            else
            {
                Console.Clear();
                Console.WriteLine("Access denied, tap any key to continue");
                Console.ReadKey();
            }
        }

        public void MoneyAmount() {
            Console.Clear();
            if (controlledAcc.IsLogged())
            {
                Console.WriteLine("Money amount: " + controlledAcc.MoneyAmount() + ", tap any key to continue");
                Console.ReadKey();
            }
            else
            {
                Console.WriteLine("You are not logged in");
                Console.ReadKey();
            }
        }

        public void ZeroEarned() {
            controlledAcc.ZeroEarned();
        }

        public void TakeEarnedMoney() {
            Console.WriteLine("Enter amount of money");
            string cashS = Console.ReadLine();
            double cash = Double.Parse(cashS);
            double res = controlledAcc.TakeEarnedMoney(cash);
            if (res != -1)
            {
                CashPutBill cpb = new CashPutBill();
                cpb.SetHeader(controlledAcc.GetAccLog());
                cpb.SetContent(cash.ToString());
                cpb.SetFooter();
                Bill bill = cpb.DispatchBill();
                bill.Print();
                Console.WriteLine("Tap any key to continue");
                Console.ReadKey();
            }
            else
            {
                Console.Clear();
                Console.WriteLine("Unaccessible operation, please check are you logged in and have enough money" + ", tap any key to continue");
                Console.ReadKey();
            }
        }

        public void EarnedMoneyAmount() {
            Console.Clear();
            if (controlledAcc.IsLogged())
            {
                Console.WriteLine("Earned money amount: " + controlledAcc.EarnedMoneyAmount() + ", tap any key to continue");
                Console.ReadKey();
            }
            else
            {
                Console.WriteLine("You are not logged in");
                Console.ReadKey();
            }
        }

        public void PutMoney() {
            Console.WriteLine("Enter amount of money");
            string cashS = Console.ReadLine();
            double cash = Double.Parse(cashS);
            double res = controlledAcc.PutMoney(cash);
            if (res != -1)
            {
                CashPutBill cpb = new CashPutBill();
                Bill bill = dir.GenerateBill(cpb, controlledAcc.GetAccLog(), cash.ToString());
                bill.Print();
                Console.WriteLine("Tap any key to continue");
                Console.ReadKey();
            }
            else
            {
                Console.Clear();
                Console.WriteLine("Unaccessible operation, please check are you logged in account");

                Console.WriteLine("Tap any key to continue");
                Console.ReadKey();
            }
        }

        public void TakeMoney() {
            Console.WriteLine("Enter amount of money");
            string cashS = Console.ReadLine();
            double cash = Double.Parse(cashS);
            double res = controlledAcc.TakeMoney(cash);
            if (res != -1)
            {
                CashTakeBill ctb = new CashTakeBill();
                Bill bill = dir.GenerateBill(ctb, controlledAcc.GetAccLog(), cash.ToString());
                bill.Print();
                Console.WriteLine("Tap any key to continue");
                Console.ReadKey();
            }
            else
            {
                Console.Clear();
                Console.WriteLine("Unaccessible operation, please check are you logged in and have enough money");
            }
        }

        public void Update() {
            controlledAcc.Update();
        }

        public string GetCommand()
        {
            string command = Console.ReadLine();
            return command;
        }

        public void RecognizeCommand(string command)
        {
            if(command.ToLower() == "log in" && !userSystem.IsLogged())
            {
                Login();
            }
            else if (command.ToLower() == "log in acc" && !controlledAcc.IsLogged())
            {
                AccLogin();
            }
            else if (command.ToLower() == "logout acc" && controlledAcc.IsLogged())
            {
                AccLogout();
            }
            else if (command.ToLower() == "deposit prop" && controlledAcc.IsLogged() && userSystem.IsLogged())
            {
                AddAccProp();
            }
            else if (command.ToLower() == "logout" && userSystem.IsLogged())
            {
                Logout();
            }
            else if (command.ToLower() == "create acc" && userSystem.IsLogged())
            {
                CreateAcc();
            }
            else if (command.ToLower() == "create worker" && userSystem.IsLogged())
            {
                CreateWorker();
            }
            else if (command.ToLower() == "print user info" && userSystem.IsLogged())
            {
                PrintUsInfo();
            }
            else if(command.ToLower() == "put money" && controlledAcc.IsLogged())
            {
                PutMoney();
            }
            else if (command.ToLower() == "take money" && controlledAcc.IsLogged())
            {
                TakeMoney();
            }
            else if (command.ToLower() == "take earned money" && controlledAcc.IsLogged())
            {
                TakeEarnedMoney();
            }
            else if (command.ToLower() == "money amount" && controlledAcc.IsLogged())
            {
                MoneyAmount();
            }
            else if (command.ToLower() == "earned money amount" && controlledAcc.IsLogged())
            {
                EarnedMoneyAmount();
            }
            else if (command.ToLower() == "logout acc" && controlledAcc.IsLogged())
            {
                AccLogout();
            }
            else if (command.ToLower() == "print acc info" && controlledAcc.IsLogged())
            {
                PrintAccInfo();
            }
            else if (command.ToLower() == "update system" && userSystem.IsLogged())
            {
                Update();
            }
            else
            {
                Console.WriteLine("Invalid command, tap any key to continue");
                Console.ReadKey();
            }
        }

        public void PrintUI()
        {
            if(!userSystem.IsLogged() && !controlledAcc.IsLogged())
            {
                Console.Clear();
                Console.WriteLine("Welcome to the BankSystem v 2.3! \nPlease, log in, if you are a worker,\n or log in your account, if you are a client");
                Console.WriteLine("Commands: log in acc, log in");
            }
            else if(userSystem.IsLogged() && controlledAcc.IsLogged())
            {
                Console.Clear();
                Console.WriteLine("Welcome, worker {0}!", userSystem.UserName());
                Console.WriteLine("Commands: create worker, create acc, add property to deposit, logout, update system");
                Console.WriteLine("You can manage your acc now");
                Console.WriteLine("Commands: put money, take money, take earned money, money amount, logout acc, print acc info");
            }
            else if (userSystem.IsLogged())
            {
                Console.Clear();
                Console.WriteLine("Welcome, worker {0}!", userSystem.UserName());
                Console.WriteLine("Commands: create worker, create acc, add property to deposit, logout, update system");
            }
            else if (controlledAcc.IsLogged())
            {
                Console.Clear();
                Console.WriteLine("You can manage your acc now");
                Console.WriteLine("Commands: put money, take money, take earned money, money amount, logout acc, print acc info");
            }
        }
    }
}
