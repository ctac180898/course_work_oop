﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Course_work_test
{
    class Program
    {
        static void Main(string[] args)
        {
            
            BankSystem bs = BankSystem.BankSys();
            string command = "none";
            while(command != "exit")
            {
                bs.PrintUI();
                command = Console.ReadLine();
                bs.RecognizeCommand(command);
            }
            Console.ReadKey();
        }
    }
}
