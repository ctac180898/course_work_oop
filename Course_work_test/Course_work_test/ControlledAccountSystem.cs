﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Course_work_test
{
    class ControlledAccountSystem : AbstAccSystem
    {
        AccountSystem system;

        public ControlledAccountSystem()
        {
            system = new AccountSystem();
        }

        override
        public bool CreateAcc(string accType, string log, string pass, double percent)
        {
            if (accType == "deposit" || accType == "default")
            {
                if (log.Length > 3 && pass.Length > 3)
                    return system.CreateAcc(accType, log, pass, percent);
                else
                    return false;
            }
            else
                return false;
        }

        override
        public bool IsLogged()
        {
            return system.IsLogged();
        }

        override
        public bool AddAccProp(string propType)
        {
            if (propType == "OutOfTurnMoneyTakeDep" || propType == "CommonDep")
            {
                return system.AddAccProp(propType);
            }
            return false;
        }

        override
        public void Logout()
        {
            system.Logout();
        }

        override
        public void PrintLoggedAccInfo()
        {
            system.PrintLoggedAccInfo();
        }

        override
        public void PrintAllAccsInfo()
        {
            system.PrintAllAccsInfo();
        }

        override
        public bool Login(string log, string pass)
        {
            if (log != "DefLog" && pass != "DefPass")
            {
                return system.Login(log, pass);
            }
            else
                return false;
        }


        override
        public double MoneyAmount()
        {
            return system.MoneyAmount();
        }


        override
        public void ZeroEarned()
        {
            system.ZeroEarned();
        }


        override
        public double TakeEarnedMoney(double cash)
        {
            return system.TakeEarnedMoney(cash);
        }


        override
        public double EarnedMoneyAmount()
        {
            return system.EarnedMoneyAmount();
        }


        override
        public double PutMoney(double cash)
        {
            return system.PutMoney(cash);
        }

        override
        public double TakeMoney(double cash)
        {
            return system.TakeMoney(cash);
        }


        override
        public string GetAccLog()
        {
            return system.GetAccLog();
        }

        override
        public void Update()
        {
            system.Update();
        }

    }
}
