﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Course_work_test
{
    abstract class AbstAccSystem
    {
        public abstract bool CreateAcc(string accType, string log, string pass, double percent);
        public abstract bool IsLogged();
        public abstract bool AddAccProp(string propType);
        public abstract void Logout();
        public abstract void PrintLoggedAccInfo();
        public abstract void PrintAllAccsInfo();
        public abstract bool Login(string log, string pass);
        public abstract double MoneyAmount();
        public abstract void ZeroEarned();
        public abstract double TakeEarnedMoney(double cash);
        public abstract double EarnedMoneyAmount();
        public abstract double PutMoney(double cash);
        public abstract double TakeMoney(double cash);
        public abstract string GetAccLog();
        public abstract void Update();
    }

    class AccountSystem : AbstAccSystem
    {
        public enum AccSystemStateSetting
        {
            Logged,
            NotLogged
        };

        private int loggedIndex;
        private InterfaceState CurrentState;
        LoggedState loggedState = new LoggedState();
        NotLoggedState notLoggedState = new NotLoggedState();

        Accs accounts;

        public AccountSystem()
        {
            accounts = new Accs();
            CurrentState = notLoggedState;
        }

        override
        public bool CreateAcc( string accType, string log, string pass, double percent)
        {

            Account acc = accounts.accs.Find(x => x.CheckAcc(log, pass));
            if (acc != null && acc.CheckAcc(log, pass))
            {
                return false;
            }
            else
            {
                Creator cr = new Creator();
                accounts.accs.Add(cr.NewAcc(accType, log, pass, percent));
                return true;
            }
        }

        override
        public bool IsLogged()
        {
            return CurrentState == loggedState;
        }

        override
        public bool AddAccProp(string propType)
        {
            if (loggedState.LoggedAcc is Deposit)
            {
                if (propType == "OutOfTurnMoneyTakeDep")
                {
                    OutOfTurnMoneyTakeDep ootmd = new OutOfTurnMoneyTakeDep();
                    ootmd.AddAccProp(accounts.accs[loggedIndex]);
                    CurrentState.LoggedAcc = ootmd;
                    accounts.accs.Insert(loggedIndex, ootmd);
                    accounts.accs.RemoveAt(loggedIndex + 1);
                    return true;
                }
                else if (propType == "CommonDep")
                {
                    CommonDep cd = new CommonDep();
                    cd.AddAccProp(accounts.accs[loggedIndex]);
                    CurrentState.LoggedAcc = cd;
                    accounts.accs.Insert(loggedIndex, cd);
                    accounts.accs.RemoveAt(loggedIndex + 1);
                    return true;
                }
            }
            return false;
        }

        override
        public void Logout()
        {
            CurrentState.LoggedAcc = null;
            CurrentState = notLoggedState;
        }

        override
        public void PrintLoggedAccInfo()
        {
            if(loggedState.LoggedAcc is Deposit || loggedState.LoggedAcc is CommonDep || loggedState.LoggedAcc is OutOfTurnMoneyTakeDep)
                Console.WriteLine("Money: {0} Earnder money: {1} Acc type: {2} Percentage: {3}", CurrentState.MoneyAmount(), CurrentState.EarnedMoneyAmount(), CurrentState.LoggedAcc.GetType(), CurrentState.LoggedAcc.Percent());
            else
                Console.WriteLine("Money: {0} Acc type: {1} ", CurrentState.MoneyAmount(), CurrentState.LoggedAcc.GetType());
        }

        override
        public void PrintAllAccsInfo()
        {
            int i = 0;
            foreach (Account acc in accounts.accs)
            {
                Console.WriteLine(i);
                acc.Print();
                i++;
            }
        }

        override
        public bool Login(string log, string pass)
        {
            if (CurrentState == notLoggedState)
            {
                Account acc = accounts.accs.Find(x => x.CheckAcc(log, pass));
                if (acc != null)
                {
                    CurrentState = loggedState;
                    CurrentState.ChooseAcc(acc);
                    loggedIndex = accounts.accs.FindIndex(x => x.CheckAcc(log, pass));
                    return true;
                }
                return false;
            }
            else
                return false;
        }


        override
        public double MoneyAmount()
        {
            if (CurrentState == loggedState)
                return CurrentState.MoneyAmount();
            return -1;
        }


        override
        public void ZeroEarned()
        {
            if (CurrentState == loggedState)
                CurrentState.ZeroEarned();
        }


        override
        public double TakeEarnedMoney(double cash)
        {
            if (CurrentState == loggedState)
                return CurrentState.TakeEarnedMoney(cash);
            return -1;
        }


        override
        public double EarnedMoneyAmount()
        {
            if (CurrentState == loggedState)
                return CurrentState.EarnedMoneyAmount();
            return -1;
        }


        override
        public double PutMoney(double cash)
        {
            if (CurrentState == loggedState)
                return CurrentState.PutMoney(cash);
            return -1;
        }


        override
        public double TakeMoney(double cash)
        {
            if (CurrentState == loggedState)
                return CurrentState.TakeMoney(cash);
            return -1;
        }


        override
        public string GetAccLog()
        {
            return loggedState.GetAccLog();
        }

        override
        public void Update()
        {
            foreach (Account acc in accounts.accs)
            {
                acc.Update();
            }
        }
    }
}
