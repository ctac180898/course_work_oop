﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Course_work_test
{
    abstract class InterfaceState
    {

        public Account LoggedAcc;
        protected string strStatename;
        public abstract double MoneyAmount();
        public abstract void ZeroEarned();
        public abstract double TakeEarnedMoney(double cash);
        public abstract double EarnedMoneyAmount();
        public abstract double PutMoney(double cash);
        public abstract double TakeMoney(double cash);
        public abstract void Update();
        public abstract Type Type();
        public abstract void ChooseAcc(Account acc);
    }

    class LoggedState : InterfaceState
    {
        public LoggedState()
        {
            strStatename = "Logged";
        }

        override
        public void ChooseAcc(Account acc)
        {
            LoggedAcc = acc;
        }

        override
        public double MoneyAmount()
        {
            return LoggedAcc.MoneyAmount();
        }

        override
        public void ZeroEarned()
        {
            LoggedAcc.ZeroEarned();
        }

        public string GetAccLog()
        {
            return LoggedAcc.GetAccLog();
        }

        override
        public double TakeEarnedMoney(double cash)
        {
            return LoggedAcc.TakeEarnedMoney(cash);
        }

        override
        public double EarnedMoneyAmount()
        {
            return LoggedAcc.EarnedMoneyAmount();
        }

        override
        public double PutMoney(double cash)
        {
            return LoggedAcc.PutMoney(cash);
        }

        override
        public double TakeMoney(double cash)
        {
            return LoggedAcc.TakeMoney(cash);
        }

        override
        public void Update()
        {
            LoggedAcc.Update();
        }

        override
        public Type Type()
        {
            return LoggedAcc.GetType();
        }
    }

    class NotLoggedState : InterfaceState
    {
        public NotLoggedState()
        {
            strStatename = "NotLogged";
        }

        override
        public void ChooseAcc(Account acc) { }

        override
        public double MoneyAmount()
        {
            return -1;
        }

        override
        public void ZeroEarned() { }

        override
        public double TakeEarnedMoney(double cash)
        {
            return -1;
        }

        override
        public double EarnedMoneyAmount()
        {
            return -1;
        }

        override
        public double PutMoney(double cash)
        {
            return -1;
        }

        override
        public double TakeMoney(double cash)
        {
            return -1;
        }

        override
        public Type Type()
        {
            return LoggedAcc.GetType();
        }

        override
        public void Update() { }
    }
}
